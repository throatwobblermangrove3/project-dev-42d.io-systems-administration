#!/bin/bash

# Setup bridged networking for KVM in Ubuntu 20.04

cd /etc/netplan
# make backup
echo "making a backup copy in /etc/netplan"
sudo cp 01-network-manager-all.yaml 01-network-manager-all.yaml.orig
ls -ltrh

sleep 4

echo "Using vim to open the file named 01-network-manager-all.yaml"
echo "Be sure to replace enp0s3 (the interface name) with your actual wired interface name if it’s different"
sleep 5

sudo vim 01-network-manager-all.yaml



