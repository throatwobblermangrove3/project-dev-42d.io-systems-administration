#!/bin/bash

echo "Verifying that your processor supports hardware virtualization"
echo
echo
echo "If the CPU supports hardware virtualization, the command will output a number greater than zero, which is the number of the CPU cores. Otherwise, if the output is 0 it means that the CPU doesn’t support hardware virtualization."
echo
echo

grep -Eoc '(vmx|svm)' /proc/cpuinfo

echo "Verifying if VT is enabled in BIOS"

sudo apt update
sudo apt install cpu-checker
kvm-ok

#End Verification


echo "Installing QEMU KVM virtualizaton using apt...."
echo "Sleeping 3 seconds"
sleep 3

sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager

echo "Assigining current user to user groups to run QEMU LIBVIRT KVM virtulization"
echo "Sleeping 3 seconds"
sleep 3

sudo usermod -a -G libvirt-dnsmasq `whoami`
sudo usermod -a -G libvirt `whoami`
sudo usermod -a -G kvm `whoami`
sudo usermod -a -G lxd `whoami`


echo "Post Installation Check for successful installation"
echo "Sleeping 3 seconds"
sleep 3

sudo systemctl is-active libvirtd
sudo systemctl status libvirtd.service

# echo "Using wget to grab a dev001-base vm image from server dev001.42d.io "
